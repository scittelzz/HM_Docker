﻿FROM mcr.microsoft.com/dotnet/sdk:7.0 as build-env
WORKDIR /src
COPY . .
RUN dotnet publish Otus.Teaching.PromoCodeFactory.sln -c Release -o /app/publish

FROM mcr.microsoft.com/dotnet/aspnet:7.0 as runtime
WORKDIR /app/publish
COPY --from=build-env /app/publish .
EXPOSE 80
EXPOSE 443

ENTRYPOINT ["dotnet", "Otus.Teaching.PromoCodeFactory.WebHost.dll"]

